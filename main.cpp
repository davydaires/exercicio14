#include <stdio.h>
#include <stdlib.h>


void quicksort(int *a, int left, int right);

// Come�o da M�gica
int main(int argc, char** argv)

#define MAX 10
{
 int i, vet[MAX];
 printf("***ESTE PROGRAMA LE 10 POSICOES E FAZ A ORDENACAO QUICKSORT***\n\n");

 for(i = 0; i < MAX; i++)

 {


  printf(" Digite um valor: ");
  scanf("%d", &vet[i]);
 }


 quicksort(vet, 0, MAX - 1);


 printf("Posicoes Ordenadas ");
 for(i = 0; i < MAX; i++)
 {
  printf("%d ", vet[i]);
 }
 system("pause");
 return 0;
}

// A magica acontece aqui = Quiksort

void quicksort(int *a, int left, int right) {
    int i, j, k, y;

    i = left;
    j = right;
    k = a[(left + right) / 2];

    while(i <= j) {
        while(a[i] < k && i < right) {
            i++;
        }
        while(a[j] > k && j > left) {
            j--;
        }
        if(i <= j) {
            y = a[i];
            a[i] = a[j];
            a[j] = y;
            i++;
            j--;
        }
    }

    if(j > left) {
        quicksort(a, left, j);
    }
    if(i < right) {
        quicksort(a, i, right);
    }
}
